# Digital Workflow Manager

[[_TOC_]]


> Do we preserve packages with multiple digital objects in them?

## Definitions
-------------------------------------------------------------------------------

### Conventions
The following conventions apply to the specifications below:
1. the words ‘shall’ and ‘must’ imply a binding and verifiable specification;
1. the word ‘should’ implies an optional, but desirable, specification; 
1. the word ‘may’ implies an optional specification; 
1. the words ‘is’, ‘are’, and ‘will’ imply statements of fact.

### Terms
- Object (or Digital Object or Digital Asset or Representation or Item): A discrete unit of information such as a file or bitstream, separate from any metadata, to be managed and preserved digitally. This includes anything that exists in a binary format including but not exclusive to: digital documents, audible content, motion picture, and other relevant digital data.
- Compound object: a set of files with a hierarchical relationship, associated with a single descriptive metadata record. For example, a scrapbook would be a complex object.
- Metadata: Data that describes an associated object. Types of metadata include descriptive, technical, structural, and preservation metadata.
- Collection: a named grouping of bibliographic items. Metadata schemas should be configurable at the collection level. Grouping is usually based on some common characteristic, such as provenance or subject. 
- Package: An object or compound object together with all associated metadata including descriptive, technical, structural, and preservation metadata. Examples include Web ARChives (WARC), and Submission Information Packages (SIP).
- File: named and ordered sequence of bytes that is known to an operating system. A File can be zero or more bytes and has a File format, access permissions, and File system characteristics such as size and last modification date.
- Bitstream: a contiguous or non-contiguous data within a file that has meaningful common properties. A Bitstream cannot be transformed into a standalone file without the addition of file structure (headers, etc.) and/or reformatting the Bitstream to comply with some particular file format.
- Public User Interface: A web based user interface that allows consumers to browse, discover and access published objects and metadata.
- Repository: A distinct system that manages some combination of objects and/or metadata for bibliographic items.
- Work (or Intellectual Entity):  a distinct intellectual or artistic creation.
- Master Copy: The original and highest quality version of an object. The master copy is also the preservation copy and is the object used to make derivatives.

### User Role
- Asset Manager: A user who can load and publish assets into the system.
- Administrator: A user that who can act in any user role and can manage the roles assigned to other users.
- Cataloger: An individual who specializes in creation of metadata, including metadata for digital objects.
- Consumer: a user who takes in published content through via the public user interface.
- Curator: Those responsible for collecting items and determining what items should be digitized and digitally preserved.
- Preservation Officer: An individual responsible for establishing and ensuring compliance with preservation standards and best practices.
- Staff User: A user with privileges to act in a role beyond that of a consumer.
- Technical Support Staff: Any IT staff that assist in implementing or supporting the operation of the system.
- User: A generic term for any person who uses the system in any way.

### Standards
- PREMIS Data Model [version 3.0](https://www.loc.gov/standards/premis/v3/premis-3-0-final.pdf)
- [Reference Model For An Open Archival Information System(OAIS)](https://public.ccsds.org/Pubs/650x0m2.pdf)
- Trusted Digital Repositories [2007](https://www.crl.edu/sites/default/files/d6/attachments/pages/trac_0.pdf), [2002](https://www.oclc.org/content/dam/research/activities/trustedrep/repositories.pdf).
- [Oxford Common File Layout](https://ocfl.io/)
- iPres
- DPC Handbook
- DCC Reference Manual
- NDSA Preservation Levels

### Metadata Standards
- Dublin Core
- MARC
- Describing Archives: A Content Standard (DACS)
- Encoded Archival Description

### Related Systems
- Library Catalog: A system the manages metadata and workflow for bibliographic items (books, maps, media, etc). Workflows include acquisitions, cataloging, circulation, serials management and others. Typically metadata is stored in the MARC format.
- Institutional Repository (IR): An archive for collecting, preserving, and disseminating digital copies of the intellectual output of an institution, particularly a research institution.
- Archival Collection Management System (ACMS): allow individuals or collecting institutions to organize, control, and manage special collections items. Typically metadata is stored in the EAD format.
- Internet Archive
- Web Archive Manager

### Sources for digital preservation

1.	Digital Image collections from the Digital Lab, CONTENTdm, etc.
2.	Historical content from University Archives and Records Management
3.	Historical and Archival Video Collections (HBLL, KBYU, etc.)
4.	Historical and Archival Audio Collections (oral histories, etc.)
5.	Collected Born-Digital gifts, donations, and acquisitions 
6.	ScholarsArchive / Institutional Repository (ETDs, publications, documents, etc.)
7.	Files added by HBLL Digital Lab to our  Internet Archive space
8.	Selected Web sites / warc files (Institution, Collection development, etc.)


## Objects

> What file types will your DAM tool support?<br />
> How does your tool account for duplicate files?<br />
> How will file names be managed?

1. The repository shall support objects in common digital formats, including but not limited to:
    1. Images (TIFF, RAW, GIF, JPG, EPS, AI, PNG)
    1. Videos (AVI, WMV, MOV, H264, MP4)
    1. Audios (WAV, AIF, WMA, AAC, MP3)
    1. Documents (PDF, DOC/X, RTF, XLS/X, PPT/X)
1. The repository shall support trans-coding of objects (e.g., AI to EPS to JPG, etc.).
1. The repository shall support compound objects.


## Metadata
> What character encodings need to be supported?

1. Each object shall have an associated metadata record.
1. Object metadata shall include, but not be limited to, data elements including: descriptive, technical, and rights information.
1. The properties of metadata fields shall be supported in all aspects of the system, including ingest, persistence, export, APIs, display, indexing and search.
1. Each field shall be mapped to a dublin core field for record export.
1. The system may have the capacity to describe content in a variety of languages.
1. System metadata fields shall support diacritics, including searching via a romanized variation.
1. System metadata fields shall support the complete ALA character set.
1. System metadata fields shall support for repeating fields.
1. System metadata fields shall support complex data types.
1. System metadata fields shall support unicode characters.
1. System metadata fields shall support customization of required and non-required fields per schema
1. System metadata fields shall support ISO 8601 dates and durations
1. System metadata fields shall support URLs as metadata
1. System metadata fields should support representing numbers as a primitive type.
1. Each metadata record shall have a unique identifier that does not change.
1. The system shall allow for externally minted Persistent Identifiers (DOIs, ARKs) to be added to a given metadata record.
- Preservation Metadata

## Controlled Vocabulary

1. The system shall enable selected metadata fields to be be limited to values found in a controlled vocabulary
1. The system shall allow import and management of customized controlled vocabularies.
1. The system shall support integration with external controlled vocabularies, including authoritative vocabulary terms/headings from Library of Congress, Getty, Visual Resource Association, and Thesaurus of Geographic Names.
- Customizable controlled vocabulary (fields, taxonomy, meta-data, etc.)
- An unlimited number of customized meta-data fields
- Customizable required and non-required asset fields
- Customized fields (meta-data) by folder/program
- Permissions granularity to allow specific users to view specific meta-data fields
- Bulk edit asset controlled vocabulary

## Collections

> Describe how your DAM product allows users to organize assets within the tool.<br />

1. The repository shall support zero or more objects organized into named (mutually exclusive?) groups called collections.
1. The name of the collection an object is currently associated with shall be part of the objects metadata.
1. A collection shall have a custom metadata schema.
1. A collection shall Support over 256 customized metadata fields.
1. The system shall not limit the creation of collections with the only exception limiting based on storage constraints.
1. The system shall allow a staff user to move items between collections.
1. The system shall allow a staff user to merge collections.
- Support PREMIS, METS, DC, and updates or modifications to the standards, e.g. PREMIS 3.


## Ingest

> How does your tool allow for file uploading, downloading, batch uploading and file sharing?<br />
> How does your tool manage video uploads? Does it retain video quality and resolution? What video file formats does it account for?<br />
> Automate ingest from various sources?

1. Staff access permits loading, deleting, and replacing single and bulk objects or descriptive metadata.
1. Upload individual content files -- or batch upload multiple content files -- with minimal to almost no metadata (e.g., file names only), and subsequently come back and add more robust metadata to the objects This is to support workflows where scanning or ingesting born digital objects in advance of being able to catalog the resources is necessary
1. Metadata embedded in content files shall be extracted on import into the system.
1. The system may allow public contributions will go into a queue so that files may be checked and verified
1. The system shall support curator and staff users to ingest packages without help from Digital Preservation Officer.
1. Upload individual content files -- or batch upload multiple content files -- and simultaneously upload and associate metadata records 
1.	Provide ingest interfaces from:
    1.	Web browser with manual metadata entry
    1.	Upload utility for objects and metadata in .csv/xls format
    1.	Ability to ingest and backup very large objects without splitting files
    1.	Work with other tools (e.g. BitCurator, ArchiveSpace)
1.	Ability to retain SIP information for ingested content 
1.	Ability to ingest a group or collection of objects and retain the group identity
1.	Ability to ingest content based on producer, content source or workflow.
1.	The system shall determine and carry out ingest tasks based on source, workflow, format, storage characteristics, collection.
1.	If configured, the system shall rename files using a naming conventions.
1.	Ingest tasks can include:
    1.	assign collection / de-assign collection
    1.	fixity check (md5, SHA1, etc)
    1.	validate fixity of objects being ingested if fixity included in metadata
    1.	validate metadata standard, such as METS, DC, etc
    1.	Index content for full searching
    1.	Create derivative copy
    1.	Create thumbnail
    1.	Assign access rights to objects 
    1.	Create persistent ID (handle, DOI, etc)
    1.	Virus check
    1.	Manage bitstreams within files (zip, tar, etc) 
    1.	Generate risk category
1.	Transform objects to a pre-determined preservation format, by risk and by format
1.	Ingest objects from repositories on campus or from other institutions
1.	Verify files on ingest, eg. JHove, PRONOM, DROID, Exiftool, virus check, etc.
1.	Verify checksum of original object and ingested object
    1.	Allow for different algorithms 
    1.	Ability to use multiple fixity algorithms for ingests
1.	Utilities to support automatic ingest, with test feature and with possible administrative review
1.	Ability to extract metadata (technical/administrative/preservation/intellectual property rights) from object on ingest
1.	Ability to manage ingest errors, format, file fixity, authenticity, and return to content creator if needed
1.	Ability to work with content producers and transfer content from them to the Digital Archive in a verified SIP
1.	Retain submission agreement and digital preservation decision form as part of the package.
1.	Tools for content producers to manage preservation objects and metadata in order to add them to the archive
1.	The system shall process packages that are uploaded to a configuring a file system location.
1.	Ability to set storage limits by producer
1.	Ability to set archive parameters by producer entity, e.g. HBLL, Archives, ScholarsArchive, etc.
- Processing Queue

## Export

- Ability to easily generate metadata exports in other standardized XML formats
- Easily generate a metadata export in a simple format, such as CSV or tab delimited files, for local re-use purposes
- Metadata and digital objects exposed as an OAI service; metadata fields tbd.
- Complete export in XML or JSON of all metadata and objects at different levels, including collection, collection subset (compound object, etc.), or individual items. (Needed for migration or external indexing.)

## Data Management

- A web interface for adding/applying metadata to content files, and for editing those metadata records
- Configure resolution at which objects are displayed, downloaded, and stored. (e.g. Full resolution, viewer, thumbnails.)
- Define user accounts with different permission levels (e.g., full admin privileges, metadata editing only, etc.), for different staff that will be creating digital objects (e.g., student assistants, etc.)
- Ample digital asset storage space for each campus (allocation to be determined)
- Access to user statistics (e.g., number of hits, terms queried, location)
- Access to campus repository statistics (e.g. number of items in a collection, number of collections)
- The system should support assigning different parts of the workflow to internal experts (e.g., metadata to a metadata librarian)
- Ability to edit metadata records for individual objects
- Ability to edit metadata records for a batch of digital objects (e.g., search and replace on data in particular metadata elements; replace data across some subset of objects in a given metadata element)
- Ability to batch edit groups of digital objects (e.g., search and replace; update data values for a particular metadata element) for quick, global updates to objects (e.g., correct typos, change repository name)
- Staff access for administrative functions (adding and modifying descriptive metadata and objects) through a web browser.

## Batch Processing

- Staff access permits batch editing. (Mass loading of transcripts after the digital objects are uploaded, mass editing of descriptive metadata, etc.)
- Bulk find and replace on metadata
    - across multiple records
    - limit by specified fields, collections
    - use wildcards and regular expressions


## Metadata Change Control
- 






## Master Copy and Derivatives

> How is the master copy managed?
> How does your tool allow for editing of media to create different versions of media assets?<br />
> Describe your solution’s capabilities to convert file types.<br />
> How does your tool compress file sizes for optimized use on web platforms?<br />
> What constitutes a derivative? Display version, thumbnail, etc<br />
> Are derivatives created automatically?<br />
> Can this be configured by collection, type, etc?<br />

1. The system shall manage a preservation/master copies of content files (e.g., TIFFs for image based objects).
1. The system shall allow staff users to automatically create derivative/service copies of the master copy (e.g., JPEG or GIF thumbnail images).


## Publication, Access and Permissions

> The tool must be accessible by anyone with access via the internet. How do users login and access the media stored in your tool?<br />
> Does your tool allow users to access the system via different mobile devices and their operating systems? Is it mobile-user friendly? Is there an app to access your tool?<br />
> The tool must have different levels of permissions for different users within the university. How will your solution provide different levels of access to users and what are the differences between those access levels?<br />
> How does your tool allow for the management of user types?<br />
> How does your tool allow for permission based access to specific media or assets within the tool? <br />
> Rights Management (Intellectual Property Rights, Embargo dates, Expiration dates) configured by object, user, collection, other?

1. All packages shall be designated for "preservation and publication" or "preservation only"
1. The system shall allow for published and unpublished digital objects
1. Unpublished digital objects shall only be available for viewing to curators and staff (catalogers, etc.) using the system to create and manage objects before making them publicly available. 
1. Assets can be downloaded in multiple sizes/formats.
1. The system shall control the degree of user access to a published digital object, based on the following:
    - User ID
    - Group or university administrative position
    - IP Address, or range (e.g. on campus only)
    - Time period (e.g. no access for 25 years)
    - Item / collection and parameter to traverse collections or restrict
    - Copyright restriction
    - Access rights parameter 
    - Moving wall for embargoed collections or content
- Allow for change to access controls (and resulting publication status) for digital objects in real time, in response to a copyright complaint or when an embargo ends.
- Dedicated url per asset
- SEO-friendly URLs that rely unique identifiers
- Identify which records from a collection were added, removed or modified since last harvest.
- A set of API's to interact with the repository where the digital objects are being managed.
- Published digital objects to display metadata records, so users have complete descriptive, rights, etc. information for an informed use of the resource.
- Published digital objects to indicate what collection(s) they are associated with.
- Permissions granularity shall allow specific users to view specific metadata fields
- Configure printing permissions.
 

## Viewer

> Can it be embedded on other webpages?<br />
> How are compound objects handled?<br />
> How are transcripts viewed? Are transcripts searchable?<br />
> Can objects be viewed as a book? Two pages at once?<br />
> Are users able to deep link into a specific page or part of an object?


- A viewer for all formats of objects will 
- Access is via a web browser without requiring a plug-in
- Access does not require a plug-in
- Viewer supports compound objects
- Object viewer options include a list of thumbnails for compound objects.

### Image Viewer

1. Image server is IIIF Compliant
1. Image viewer enables user to zoom, pan, rotate images.

### Document Viewer


### Video Streaming


### Audio Streaming
- 


## Online Catalog

> Describe how DAM users are able to share assets with different users within the tool.<br />
> Does your tool allow for a custom login page that reflects organization branding and allows for personalized user experiences for our users?<br />

- A page describing the collections in the system should be available on the site. 
- Meets LIT standards for browser compatibility.

- Allow users to create a “favorites” list.
- Allow printing of all or selected pages of a digital objects
- Allow download of all or selected pages of a digital objects
- Allow download of in various formats, e.g. PDF, txt, zip, etc.
- Digital object and derivatives available through a persistent URIs.

- Intuitive, easy navigation.
- Users can interact with content through tagging, commenting, ratings, etc. to enrich collections/content. User-created content is mediated.
- Social media sharing capabilities for non-restricted collections.

- Users should be able to email to themselves or others the objects saved to a session-based item / book bag page. 
- A link or icon should be available that when clicked will allow the user to send objects to social media targets (e.g., Facebook, Delicious, Pinterest). Attribution
- Attribution/brand should always be present; all pages should have a branding area at the top that will include at minimum the BYU Library brand.
- A link or icon should be available from all pages that when clicked provides a feedback form for submitting comments and questions to the UC Libraries Digital Collection staff.

- Objects should be displayed in a view that provides institutional branding. 
- PDFs should be displayed within the branded area and not in a separate Adobe Acrobat Reader window.
- Images should be represented by thumbnails that when clicked open to a full view of the image within an image viewer.
- Images should be easily optimized for viewing, including zoom in/out, rotate, mirror/flip, fit image, and full size.

- Search terms should be highlighted in the object view, regardless of format. 
- Items similar or related to the displayed object should be linked to from the object view page allowed users to view “more like this”. 
- The object view should display object citation information.

- Allow users to view/compare two digital objects simultaneously.
- View transcripts with digital objects.
- View transcripts side-by-side with digital objects when screen width allows.
- Objects should be accessible from a mobile devices.

- Social features (comments, likes, etc)

## Discovery

> Describe how your tool allows users to search for specific assets within the database.<br />

- Search term spelling correction should be provided. 
- Content should be discoverable and displayable via mobile devices.
- Browse all items in a collection.
- The metadata and object are discoverable through main library search.
- Search results may be viewed as a list.
- Search results may be viewed as a grid.
- Search results have option to change number of results displayed per page
- Supports searching of diacritics. ........
- Search limiting by collections, boolean operators, and other fields.
- Ability to configure any field to be searchable, including full text and hidden fields.
- Ability to highlight search terms in search results, including individual matching pages within compound objects.
- Allow sorting of search results by a configurable set of fields.
- Items can be embargoed until some specified date.
- Each item in a result set should be accompanied by the following primary metadata: title, subject, description, contributor, date, format, rights.
- Facets should serve to refine or expand search results and should be made available for the following primary metadata: genre, subject, description, contributor, date, format, rights.
- Default sorting of search results should be by relevance; users should have option to sort by additional sorting criteria: collection, author, title, date.
- Users should be provided option to display pre-set items per page (e.g., 10, 15, 20).
- Result sets should be paginated with users able to navigate back / forth through pages of results. 
- Metadata and objects should be discoverable through external search engines.
- Various ontologies of linked data is embedded into the HTML of the landing page to make the object metadata openly available. Schema.org is required. Additional ontologies can be added.
- Ability for end users to discover a specified repository's digital objects -- e.g., a browse or limit by (UCB, UCSF, UCLA, etc.) repository – for ease of pointing users to local items.

## APIs
> Describe how media stored in your tool can be accessed via API from websites.<br />

- The system shall provide RESTful APIs.
- The APIs shall allow provide access to individual 

## Events
- The system shall provide a stream of events for each object every time it is created, changed, deleted, accessed, or preserved.
- Event data shall include an id of the item, the type of event, and a timestamp for the event.





## Support and Maintenance
-------------------------------------------------------------------------------

> What kind of uptime do you typically deliver (also define any terms within your answer as appropriate)? Do you provide 24x7x365 support on a global scale? Please identify your service level agreements and include these as part of the Attachment A Pricing Schedule.<br />
> What are the biggest risks to the solution, in terms of availability (e.g., power outages, network outages, data corruption, software bugs, reliance on external partners), and how are these risks mitigated? Provide any examples you can of large outages that have occurred, how long they lasted, and how you resolved them.<br />
> Describe the parameters of your “typical” Service Level Agreement (SLA). How well does your solution meet those targets?<br />
> What support options are available for your solution after go-live?<br />
> What is your guaranteed response time for responding to emergency and non-emergency requests?<br />
> Where are your support staff located and during what hours are your support team available? What provisions do you have in place for after-hours support?<br />
> How do you facilitate and encourage support through user groups or communities of practice? What role, if any, does a user group/community of practice have in identifying and prioritizing enhancements?<br />
> What are the expectations, qualifications, and time commitments of someone managing your solution? Is a local staff member required to be in charge of managing this platform internally?<br />

1. The system shall require less than or equal to 1.5 FTE in technical support staff.



## Reliability
> Describe how your tool maintains the integrity of the original file (i.e. quality, color, resolution).<br />
> How are violations of service availability recorded?<br />
> Describe how your solution is fully fault-tolerant without a single point of failure.<br />
> Describe any redundancy features.<br />
> How does your solution monitor and report on system reliability and performance?<br />
> How are fixes and reported issues prioritized?<br />

> Are backups encrypted, and who can access them?<br />
> Is periodic testing of backup integrity performed? Describe the timetable for such testing.<br />
> How and where are backups stored? Please be specific with regard to medium and parties involved.<br />
> Describe your solution’s mechanisms for recovery.<br />
> What processes are in place for disaster management?<br />
> What is the expected time frame for a restore to occur?<br />

> How does your tool archive or otherwise treat old files?<br />

1. The system shall store packages redundantly with at least 3 copies.
1. The system shall store packages in at least 2 geographically separated locations.
1. The system should have a method for replicating items that are currently preserved to new storage.
1. The system shall allow users to retrieve objects without from technical support staff or administrator assistance.
1. The system should retain a copy of every version of metadata for each record.
1. Staff Users should be able to revert metadata to a previous version.

- Object Validation - Integrity
- Fixity
- Backups (schedule, media, copies)
- Retention schedules
- 

## Performance and Scalability

> Describe how scalable your solution is in terms of potentially adding additional users in the future. Please describe functions and features that allow such scalability without decreasing performance.<br />
> One of the main purposes of using a DAM is to be able to update content seamlessly across all of ASU’s channels. ASU consistently works on multiple projects that impact the lives of learners around the globe. Describe how your system caters to a global audience.<br />
> How should the solution manage capacity at an infrastructure level?<br />
> How should the solution accommodate increases in users and collections?<br />
> How is performance monitored?<br />
> How should the solution manage peaks and spikes in workload over varying periods of time, including seconds, minutes and hours?<br />
> How should the solution enable simultaneous batch operations across multiple institutions? Are there any restrictions on simultaneous batch operations? <br />
> Describe expected performance for batch load processes including factors affecting processing time and performance. What factors affect processing time? Can batch loads be scheduled?<br />
> Are there governance thresholds or restrictions for the import and export of data?<br />

1. The system shall have a total capacity in excess of 40 TB.
1. The system shall support periodic expansion of its total storage capacity.
1. The system shall fully support objects up to 32 GB in size.
1. The system should fully support objects up to 3 TB in size.
1. The system should set storage limits by data source.
1. The system shall propagate changes to made to an individual object and its metadata system-wide in near real-time.
1. The system should propagate bulk changes to made to objects and metadata system-wide in near real-time.
1. The system shall propagate changes to permissions and collections system-wide in near real-time.

## Hosting
1. The servers hosting the system may be locally managed or provided as a service by a 3rd party.
1. The servers hosting the system may be on premises or in the cloud (offsite).
- Hardware requirements?


## Documentation
1. A User Guide describing how to use the features the system should be available on online.
1. The User Guide should be updated regularly to keep it current and accurate for the system that is in use.
- Training

## Logging and Reporting

> Does the tool need data reporting structures on the media usage, location, and end user engagement?<br />
> How does your tool provide data about the media, and what kind of data is represented in your tool?<br />
> Does your tool allow for custom automated reports or dashboards? If so, how are users able to create these?<br />
> Should users be able to receive custom notifications when specified media is downloaded, edited, or used on a web asset?<br />
> How does your tool provide trend information about the use of media and about the quantity and type of media uploaded to the tool?<br />
> How does your tool account for files that have time-sensitive use? For example, if we have a limited license to an image for x number of months.<br />

1. Provide a dashboard that shows status of collections
1. Provide a dashboard that shows status of processes
1. Track administrative metadata for each object including index/load/update history.
1. Create an audit trail of historical asset usage.
1. Ability to generate statistical reports about usage. (page views, downloads, number of objects, etc.)
1. Ability to generate reports on administrative metadata.
1. Reports about user data including location, demographics, etc.
1. Report on asset expiration
1. Report on asset downloads by user and by date
1. Report on program specific metadata
1.	Ingests by day, month, year, total
1.	Ingests by producer, format, workflow
1.	Error files, ignored objects, rejected files
1.	Batch ingest reports for producers
1.	Report on SIPS and status
1.	Report of new items to be harvested in an existing Source collection 
1.	Report or counter of repository statistics (Day, week, month, year, total) for storage space used, files archived, Intellectual Entities archived
1.	Report of formats preserved in the archive, 
1.	Objects viewed, downloaded or exported
1.	Size of files stored
1.	Report of storage used by collection
1.	Items in risk categories by format
1.	Audit trail by objects and or collection
1.	Retention report
1.	Report by collection, including collection members, IDs, and numbers
1.	Access reports
1.	Create custom reports of content in the archive
1.	Create custom query lists for reporting and use with repository task list
1.	Provide logs or ingests / harvests 


## Security
> User interface on HTTPS?<br />
> Does any data need to be encrypted while it is stored?<br />
>


> Does the solution require security protections such as content encryption, network segmentation, etc.<br />
> Describe security requirements for data transit.<br />
> What encryption options are needed? Describe the different levels of encryption.<br />
> Describe security controls that enforce separation of duties.<br />
> Describe security controls in place for endpoint protection on systems used by your developers, system administrators, and others supporting your solution.<br />
> How will should those supporting the solution authenticate to it and how such access is monitored and logged.<br />
> Two-factor authentication?<br />

- Encryption

## Constraints and Limitations
-------------------------------------------------------------------------------

> Does the system need to support a structure that allows for multiple partnerships or interfaces with BYU partnerships or other academic partnerships?<br/>
> What is the collecting policy for digital objects? Should we create an SLA that aligns with this policy?<br/>
> Does the library want to certify as a trusted repository? https://www.crl.edu/sites/default/files/d6/attachments/pages/trac_0.pdf<br/>
> What role will the Millenniata Disk play in our workflow?<br/>
> What role will Box.com play in this solution?

### Governance
1. Governance of the system is managed by the committees Administrative Council has specifically charged with responsibility for the system.
1. The cost of any purchased system must be approved by Administrative Council.

### Licensing
1. The licensing of any purchased software should provide software code, documentation, and internal materials in escrow.
1. The licensing of any purchased software may be proprietary or open source.

